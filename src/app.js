import React, { Component } from 'react';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import reducers from './reducers';
import firebase from 'firebase';
import ReduxThunk from 'redux-thunk';
import Router from './Router';

class App extends Component {

	componentWillMount() {
		const config = {
	    apiKey: "AIzaSyBoFZwIBxuQJ80nhrkmHbuurRtPrL405a0",
	    authDomain: "reactnative-manager-2fb0f.firebaseapp.com",
	    databaseURL: "https://reactnative-manager-2fb0f.firebaseio.com",
	    projectId: "reactnative-manager-2fb0f",
	    storageBucket: "reactnative-manager-2fb0f.appspot.com",
	    messagingSenderId: "236865803044"
	  };

	  firebase.initializeApp(config);
	}

	render() {
		const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

		return (
			<Provider store={store} >
				<Router />
			</Provider>
		);
	}
}

export default App;
