// Import libraries for making a component
import React from 'react';
import { Text, View } from 'react-native';

// Make a component
const Header = (props) => {
	const { textStyle, viewStyle } = styles;

	return (
		<View style={viewStyle} >
			<Text style={textStyle}>{props.headerText}</Text>
		</View>
	);
};

const styles = {
	viewStyle: {
		backgroundColor: '#F8F8F8',
		justifyContent: 'center',
		alignItems: 'center',
		height: 60,
		paddingTop: 15,
		shadowColor: '#000',
		// shadow property for IOS
		shadowOffset: { width: 0, height: 20 },
		shadowOpacity: 0.2,
		// shadow property for android
		elevation: 5,
		position: 'relative'
	},
	textStyle: {
		fontSize: 20
	}
};


// Make the component available to other parts of the app
export { Header };